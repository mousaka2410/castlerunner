package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import java.util.EnumMap;

/**
 * Created by StianV on 31.01.2015.
 */

public class GameHero {
    public enum HeroMovement{
        RUNNING, CLIMBING, JUMPING, DUCKING, SWINGING, FALLING
    }

    public static final float JUMP_HEIGHT = 2.1f /* m */;
    public static final float WHIP_RELOAD_TIME = 0.5f /* s */;
    HeroMovement movement;
    float timeSinceMovementChange;
    private float timeSinceLastWhip;
    Rectangle position;
    Vector2 vel;
    float jumpVelY;
    float offsetX = 7.0f /* pixels */;

    private final Vector2 downCheckVel = new Vector2(0.0f, -0.5f);

    private EnumMap<HeroMovement,Animation> anim =
            new EnumMap<HeroMovement, Animation>(HeroMovement.class);

    private void makeHeroAnimation(HeroMovement movement, String filename, int nFrames,
                                   float frameDuration)
    {
        anim.put(movement, MyGdxGame.makeAnimation(filename, nFrames, frameDuration));
    }

    public GameHero(){
        jumpVelY = (float) Math.sqrt(-2 * GamePhys.GRAVITY * JUMP_HEIGHT);
        makeHeroAnimation(HeroMovement.RUNNING, "char_walkloop6_strip4.png", 4, 0.1f);
        position = new Rectangle(0.0f, 0.0f, 0.8f, 1.4f);
        vel = new Vector2();
        //corners = new Vector2[4];
        //oldCorners = new Vector2[4];
        reset();
    }

    public TextureRegion getFrame(){
        return anim.get(HeroMovement.RUNNING).getKeyFrame(timeSinceMovementChange, true);
    }

    public void tryToClimbOrJump(){
        switch (movement) {
            case RUNNING:
                setMovement(HeroMovement.JUMPING);
                vel.y = jumpVelY;
            default:
                break;
        }
    }

    public void tryToDuck(){
        switch (movement) {
            case RUNNING:
                setMovement(HeroMovement.DUCKING);
                break;
            default:
                break;
        }
    }

    private void setMovement(HeroMovement movement){
        this.movement = movement;
    }

    public boolean whipping(){
        return timeSinceLastWhip >= WHIP_RELOAD_TIME;
    }

    public void tryToWhip(float x, float y){
        if (!whipping()){
            timeSinceLastWhip = 0.0f;
        }
    }

    public void update(float dt, GameMap map){
        timeSinceMovementChange += dt;
        timeSinceLastWhip += dt;

        if (map.collidesWithFloor(position, vel, dt)){
            Gdx.app.debug("GameHero", "Touchdown! Vel: " + vel.y + " dt: " + dt);
            vel.y = 0.0f;
            setMovement(HeroMovement.RUNNING);
        }

        position.x += vel.x * dt;
        position.y += vel.y * dt;

        switch (movement){
            case JUMPING:
            case FALLING:
                vel.y += GamePhys.GRAVITY *dt;
                break;
            case RUNNING:
            case DUCKING:
                // TODO: check something à la map.collidesWithFloor(position, (0.0, 0.1), 0.1)
                if (!map.collidesWithFloor(position, downCheckVel, 0.1f)){
                    Gdx.app.debug("GameHero", "Nothing below.");
                    setMovement(HeroMovement.FALLING);
                }
                break;
            default:
                break;
        }
    }

    public void reset(){
        movement = HeroMovement.RUNNING;
        timeSinceMovementChange = 0.0f;
        timeSinceLastWhip = WHIP_RELOAD_TIME;
        position.x = position.y = 0.0f;
        vel.x = 3.0f /* m/s */;
        vel.y = 0.0f;
    }
}
