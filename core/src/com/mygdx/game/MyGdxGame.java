package com.mygdx.game;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class MyGdxGame extends ApplicationAdapter {
    public static final float GAME_SCREEN_WIDTH = GamePhys.metersToPixels(15.0f);

    float width;
    float height;
    boolean pause = true;
    OrthographicCamera camera;
    Rectangle display = new Rectangle();
	SpriteBatch batch;
	Texture tileSet;
    TextureRegion tile;
    GameHero hero;
    GameInputProcessor gameInput;
    GameMap map;

	@Override
	public void create () {
        Gdx.app.setLogLevel(Application.LOG_DEBUG);

		batch = new SpriteBatch();
        camera = new OrthographicCamera();
        width  = Gdx.graphics.getWidth();
        height = Gdx.graphics.getHeight();
        hero = new GameHero();
        map  = new GameMap();



        gameInput = new GameInputProcessor();
        Gdx.input.setInputProcessor(gameInput);

        resetGame();
	}

	@Override
	public void render () {
        camera.update();
        batch.setProjectionMatrix(camera.combined);
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
        /*
        for (int j=0; j<10; j++) {
            batch.draw(tile, GamePhys.metersToPixels(j), 0);
        } */
        map.render(batch, display);
        batch.draw(hero.getFrame(),
                GamePhys.metersToPixels(hero.position.x) - hero.offsetX,
                GamePhys.metersToPixels(hero.position.y));
        batch.end();

        if (pause) {
            if (gameInput.isTapped()) {
                pause = false;
            }
        } else {
            checkInput();
            update();
        }
    }

    private void checkInput() {
        if (gameInput.isTapped()){
            hero.position.x = screenToGameX(gameInput.tapX());
            hero.position.y = screenToGameY(gameInput.tapY());
            Gdx.app.debug("MyGdxGame", "HeroPosition = (" + hero.position.x +
                    "," + hero.position.y + ")");
        }

        if (gameInput.isSwipedDown()){
            hero.tryToDuck();
        }

        if (gameInput.isSwipedUp()){
            hero.tryToClimbOrJump();
        }
    }

    public float screenToGameY(float yScreen) {
        return GamePhys.pixelsToMeters((height - yScreen) * GAME_SCREEN_WIDTH / width);
    }

    public float screenToGameX(float xScreen) {
        return GamePhys.pixelsToMeters(xScreen * GAME_SCREEN_WIDTH / width);
    }

    private void update(){
        float dt = Gdx.graphics.getDeltaTime();
        hero.update(dt, map);
        //camera.translate(hero.vel.x * dt, 0.0f);
        //display.x += hero.vel.x * dt;
    }

    private void resetGame() {
        configureCamera();
        hero.reset();
        loadLevel(0);
    }

    private void configureCamera() {
        display.width = GAME_SCREEN_WIDTH;
        display.height = GAME_SCREEN_WIDTH*height/width;
        camera.setToOrtho(false, display.getWidth(), display.getHeight());
    }

    private void loadLevel(int level){
        hero.position.x = 1.0f /* m */;
        hero.position.y = 2.0f /* m */;
        String mapData[] = {
                "_____________________________________________________",
                "________X______XXXX__________________________________",
                "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX__XXXXXXXXXXXXXXXXXXXXXX",
        };

        map.setMapData(mapData);
    }

    public static Animation makeAnimation(String filename, int nFrames, float frameDuration){
        Texture animSheet = new Texture(filename);
        TextureRegion[][] tmp = TextureRegion.split(animSheet,
                animSheet.getWidth() / nFrames, animSheet.getHeight());
        return new Animation(frameDuration, tmp[0]);
    }

    static Vector2[] getCorners(Rectangle boundingBox, Vector2[] corners){
        corners[0].set(boundingBox.getX(), boundingBox.getY());
        corners[1].set(boundingBox.getX()+boundingBox.getWidth(), boundingBox.getY());
        corners[2].set(boundingBox.getX()+boundingBox.getWidth(), boundingBox.getY()+boundingBox.getHeight());
        corners[3].set(boundingBox.getX(), boundingBox.getY() + boundingBox.getHeight());

        return corners;
    }

    @Override
    public void dispose() {
        batch.dispose();
    }
}
