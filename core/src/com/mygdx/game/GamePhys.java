package com.mygdx.game;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by StianV on 01.02.2015.
 */
public class GamePhys {
    public static final float PIXELS_PR_METER   = 16.0f;
    public static final float GRAVITY = -9.81f /* m/s² */;
    public static float pixelsToMeters(float nPixels){
        return nPixels / PIXELS_PR_METER;
    }
    public static float metersToPixels(float nMeters){
        return nMeters * PIXELS_PR_METER;
    }

    public enum RectSide { LEFT, RIGHT, TOP, BOTTOM, NONE, ERROR };

    /*
    public static boolean contains(Rectangle rect, float x, float y){
        return rect.x <= x && x < rect.x+rect.width && rect.y <= y && y < rect.y+rect.height;
    }
    */

    public static RectSide intersectsSide(Rectangle box, float oldX, float oldY, float newX, float newY){
        boolean containsOld = box.contains(oldX, oldY);
        boolean containsNew = box.contains(newX, newY);
        // Are both inside?
        if (containsOld && containsNew) {
            return RectSide.NONE;
        }
        // Are both outside?
        if (!containsOld && !containsNew) {
            return RectSide.NONE;
        }
        float insideX, insideY, outsideX, outsideY;
        if (containsOld){
            insideX = oldX;
            insideY = oldY;
            outsideX = newX;
            outsideY = newY;
        } else {
            insideX = newX;
            insideY = newY;
            outsideX = oldX;
            outsideY = oldY;
        }

        float dx = insideX - outsideX;
        float dy = insideY - outsideY;
        float x, dy2, dx2;

        // if point is above box
        float topLineY = box.getY() + box.getHeight();
        if (outsideY >= topLineY){
            dy2 = topLineY - outsideY;
            // using similarity formula dx2/dy2 = dx/dy  =>  dx2 = dx*dy2/dy
            dx2 = dx*dy2/dy;
            x = dx2 + outsideX;
            if (box.getX() <= x && x <= box.getX()+box.getWidth()){
                return RectSide.TOP;
            }
        }

        // if point is below box
        if (outsideY < box.getY()){
            dy2 = box.getY() - outsideX;
            // using similarity formula
            dx2 = dx*dy2/dy;
            x = dx2 + outsideX;
            if (box.getX() <= x && x <= box.getWidth()){
                return RectSide.BOTTOM;
            }
        }

        // if point is to the left of the box
        if (outsideX < box.getX()){
            return RectSide.LEFT;
        }

        // if point is to the right of the box
        if (outsideX > box.getX()+box.getWidth()){
            return RectSide.RIGHT;
        }

        assert(false);
        return RectSide.ERROR;
    }
}
