package com.mygdx.game;

/**
 * Created by StianV on 31.01.2015.
 */

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;

/**
 * GameInputProcessor will register three events for the game application, namely if the user has
 * tapped the screen, swiped up, or swiped down. Any of these are registered when the user releases
 * the touch.
 */
public class GameInputProcessor implements InputProcessor {
    private int xDown;
    private int yDown;
    private int yDragged;
    private boolean touched;
    private boolean dragged;

    private boolean swipeUp;
    private boolean swipeDown;
    private boolean tap;

    GameInputProcessor(){
        tap = false;
        swipeUp = false;
        swipeDown = false;
    }

    boolean isTapped(){
        boolean ret = tap;
        tap = false;
        return ret;
    }

    /**
     * Should only be called after isTapped() returns <code>true</code>.
     * @return screenX coordinate for when the screen was tapped.
     */
    int tapX(){
        Gdx.app.debug("GameInputProcessor", "tapX: " + xDown);
        return xDown;
    }

    /**
     * Should only be called after isTapped() returns <code>true</code>.
     * @return screenY coordinate for when the screen was tapped.
     */
    int tapY(){
        Gdx.app.debug("GameInputProcessor", "tapY: " + yDown);
        return yDown;
    }

    boolean isSwipedUp(){
        boolean ret = swipeUp;
        swipeUp = false;
        return ret;
    }

    boolean isSwipedDown(){
        boolean ret = swipeDown;
        swipeDown = false;
        return ret;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        touched = true;
        xDown = screenX;
        yDown = screenY;
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        assert(touched == true);

        if (dragged) {
            swipeUp = yDown > yDragged;
            swipeDown = yDown < yDragged;
        } else {
            tap = true;
        }
        touched = false;
        dragged = false;

        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        assert(touched == true);
        dragged = true;
        yDragged = screenY;
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
