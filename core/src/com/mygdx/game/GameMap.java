package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by StianV on 01.02.2015.
 */
public class GameMap {
    String[] mapData;
    void setMapData(String[] mapData){
        this.mapData = mapData;
    }
    // Collision box rectangle for the tile under inspection. To be recirculated.
    private Rectangle toCheck = new Rectangle();
    static final float TILE_WIDTH = 1.0f /* m */;
    static final float TILE_HEIGHT = 1.0f /* m */;

    private final float EPSILON = 0.00001f;

    Texture wallTileSet;
    TextureRegion wallTileImg;

    GameMap(){
        wallTileSet = new Texture("8Blocks_FOREGROUND_8p.png");
        wallTileImg = new TextureRegion(wallTileSet, 0, 0, 16, 16);
    }

    Rectangle collidesWithTile(float x, float y){
        if (y >= mapData.length) return null;
        if (x >= mapData[0].length()) return null;
        char tileType = mapData[(int)y].charAt((int)x);
        switch (tileType){
            case 'X':
                return new Rectangle((int)x, (int)y, 1.0f, 1.0f);
            default:
                return null;
        }
    }

    int posToIdX(float posX){
        return (int) (posX / TILE_WIDTH);
    }
    int posToIdY(float posY){
        return (int) (posY / TILE_HEIGHT);
    }

    private void renderTile(SpriteBatch batch, int idX, int idY){
        switch(getTileType(idX, idY)) {
            case 'X':
                batch.draw(wallTileImg,
                        GamePhys.metersToPixels(idX*TILE_WIDTH),
                        GamePhys.metersToPixels(idY*TILE_HEIGHT));
                break;
            default:
                break;
        }
    }

    public void render(SpriteBatch batch, Rectangle display){
        int nTilesX = (int) (display.getWidth()/TILE_WIDTH) + 1;
        int nTilesY = (int) (display.getHeight()/TILE_HEIGHT) + 1;
        int startX = posToIdX(display.x);
        int startY = posToIdY(display.y);
        for (int idY=startY; idY<startY+nTilesY; idY++){
            for (int idX=startX; idX<startX+nTilesX; idX++){
                renderTile(batch, idX, idY);
            }
        }
    }

    private char getTileType(int x, int y){
        if (y < 0 || mapData.length <= y){
            return ' ';
        }
        if (x < 0 || mapData[y].length() <= x){
            return ' ';
        }
        return mapData[mapData.length-1 - y].charAt(x);
    }

    private Rectangle getCollisionRect(int x, int y, Rectangle other, char tileType){
        if (getTileType(x, y) == tileType){
            // Set the content of the toCheck rectangle
            toCheck.x = x*TILE_WIDTH - other.getWidth();
            toCheck.y = y*TILE_HEIGHT - other.getHeight();
            toCheck.width  = TILE_WIDTH  + other.getWidth();
            toCheck.height = TILE_HEIGHT + other.getHeight();
            return toCheck;
        }
        return null;
    }



    boolean collidesWithFloor(Rectangle pos, Vector2 vel, float dt){
        if (0 <= vel.y){
            return false;
        }

        int idX = posToIdX(pos.x);
        int idY = posToIdY(pos.y);

        for (int dy = -1; dy<1; dy++){
            for (int dx = -1; dx<3; dx++) {
                Rectangle rect = getCollisionRect(idX+dx, idY+dy, pos, 'X');
                if (rect == null) continue;
                if (rect.contains(pos.getX(), pos.getY())){
                    Gdx.app.debug("GameMap",
                            "collidesWithFloor(): object is already inside wall tile!");
                }
                if (rect.contains(pos.getX() + vel.x * dt, pos.getY() + vel.y * dt)){
                    // find out where the line between the old position and the new position
                    // intersects the box.
                    GamePhys.RectSide side = GamePhys.intersectsSide(rect, pos.getX(), pos.getY(),
                            pos.getX()+vel.x*dt, pos.getY()+vel.y*dt);
                    if (side == GamePhys.RectSide.TOP){
                        // move the object up to above the floor
                        pos.y = rect.getY() + rect.getHeight() + EPSILON;
                        return true;
                    }
                }
            }
        }
        return false;
    }

}
